package ru.aisolutions.qaservice.service;


import java.util.List;
import ru.aisolutions.qaservice.model.Attribute;

public interface AttributeService {
  List<Attribute> getAll();

  Attribute getById(Long id);
}



