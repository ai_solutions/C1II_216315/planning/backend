package ru.aisolutions.qaservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.aisolutions.qaservice.model.AttrValue;
import ru.aisolutions.qaservice.repository.AttrValueRepository;

@Service
public class AttrValueServiceImpl implements AttrValueService {
    @Autowired
    AttrValueRepository attrValueRepository;

    @Override
    public List<AttrValue> getLastAttrValuesByUserId(String userId) {
        return attrValueRepository.getLastAttrValuesByUserId(userId);
    }

    @Override
    public List<AttrValue> getAnswerByAttrLabelForUser(String userId, String attrLabel) {
        return attrValueRepository.getAttrValueHistoryByAttrId(userId, Integer.parseInt(attrLabel));
    }

    @Override
    public void saveAttrValue(String userId, Integer attrId, String value) {
        attrValueRepository.insertAttrValue(userId, attrId, value);
    }


}
