package ru.aisolutions.qaservice.service;

import java.util.List;
import ru.aisolutions.qaservice.model.AttrValue;

public interface AttrValueService {
    List<AttrValue> getLastAttrValuesByUserId(String userId);

    List<AttrValue> getAnswerByAttrLabelForUser(String attrLabel, String userId);

    void saveAttrValue(String userId, Integer attrId, String value);

}
