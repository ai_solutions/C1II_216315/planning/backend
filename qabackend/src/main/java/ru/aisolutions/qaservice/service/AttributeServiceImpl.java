package ru.aisolutions.qaservice.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.aisolutions.qaservice.model.Attribute;
import ru.aisolutions.qaservice.repository.AttributeRepository;
@Service
public class AttributeServiceImpl implements AttributeService {
  @Autowired
  AttributeRepository questionRepository;

  @Override
  public List<Attribute> getAll() {
    return questionRepository.findAll();
  }

  @Override
  public Attribute getById(Long id) {
    return questionRepository.getReferenceById(id);
  }


}
