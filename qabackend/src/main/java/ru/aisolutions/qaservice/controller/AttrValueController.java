package ru.aisolutions.qaservice.controller;


import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;
import ru.aisolutions.qaservice.dto.AttrValueDto;
import ru.aisolutions.qaservice.dto.AttrValueHistoryDto;
import ru.aisolutions.qaservice.model.AttrValue;
import ru.aisolutions.qaservice.service.AttrValueService;
import ru.aisolutions.qaservice.service.AttributeService;


@RestController
@RequestMapping("/attr-values")
public class AttrValueController {
    @Autowired
    AttrValueService attrValueService;
    @Autowired
    AttributeService attributeService;

    @GetMapping(value = "/recent-values", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Returns recent attribute values", tags = "attribute")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Found attr values.", content = {@Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = AttrValueDto.class)))})})
    public List<AttrValueDto> getLastAnswers(@AuthenticationPrincipal Jwt principal) {
        var attrValues = attrValueService.getLastAttrValuesByUserId(principal.getClaimAsString("sub"));
        var attributes = attributeService.getAll().stream().collect(Collectors.toMap(attribute -> attribute.getId(), attribute -> attribute.getLabel()));
        return attrValues.stream().map(attrValue -> new AttrValueDto(attributes.get(attrValue.getAttrId()), attrValue.getValue())).collect(Collectors.toList());
    }
    @PostMapping(value = "/save-attr-values", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Attr values saved.")})
    public void saveAttrValues(@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Receives dictionary where key = attr_id and value = attr_value.") @RequestBody Map<Integer, String> attributes, @AuthenticationPrincipal Jwt principal) {
        AttrValue attrValue;
        String userId = principal.getClaimAsString("sub");
        for (Map.Entry<Integer, String> attr : attributes.entrySet()) {
            attrValueService.saveAttrValue(userId, attr.getKey(), attr.getValue());
        }
    }

    @GetMapping(value = "/history", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Attr history returned.", content = {@Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = AttrValueHistoryDto.class)))})})
    public List<AttrValueHistoryDto> getAttrHistory(@AuthenticationPrincipal Jwt principal, @RequestParam(name = "attr_id") String attrId) {
        String currentAttrLabel = attributeService.getById(Long.parseLong(attrId)).getLabel();
        return attrValueService.getAnswerByAttrLabelForUser(principal.getClaimAsString("sub"), attrId).stream().map(attr -> new AttrValueHistoryDto(currentAttrLabel, attr.getValue(), attr.getResponseTimestamp())).toList();
    }
}
