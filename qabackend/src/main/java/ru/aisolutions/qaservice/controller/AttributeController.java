package ru.aisolutions.qaservice.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ru.aisolutions.qaservice.dto.QuestionDto;
import ru.aisolutions.qaservice.model.AttributeType;
import ru.aisolutions.qaservice.service.AttributeService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/attributes")
public class AttributeController {
    @Autowired
    AttributeService attributeService;

    @GetMapping(value = "/questions", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Returns all questions", tags = "question")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Found questions", content = {@Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = QuestionDto.class)))})})
    public List<QuestionDto> getAllQuestions() {
        return attributeService.getAll().stream().filter(attribute->attribute.getAttributeType()== AttributeType.raw_value).map(attribute -> new QuestionDto(attribute)).collect(Collectors.toList());
    }

    @GetMapping(value = "/calculated_values", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Returns attributes witch values should be calculated", tags = "calculated values")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Found questions", content = {@Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = QuestionDto.class)))})})
    public List<QuestionDto> getCalculatedAttributes() {
        return attributeService.getAll().stream().filter(attribute->attribute.getAttributeType()== AttributeType.calculated_value).map(attribute -> new QuestionDto(attribute)).collect(Collectors.toList());
    }
}
