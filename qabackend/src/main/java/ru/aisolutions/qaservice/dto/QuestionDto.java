package ru.aisolutions.qaservice.dto;

import ru.aisolutions.qaservice.model.Attribute;

public class QuestionDto {
    private int id;
    private String wording;
    private String payload;

    public QuestionDto(Attribute attribute) {
        this.id = attribute.getId();
        this.wording = attribute.getQuestionWording();
        this.payload = attribute.getQuestionPayload();
    }

    public int getId() {
        return id;
    }

    public String getWording() {
        return wording;
    }

    public String getPayload() {
        return payload;
    }
}
