package ru.aisolutions.qaservice.dto;

import java.time.Instant;

public class AttrValueHistoryDto {
    private String label;
    private String value;
    private Instant responseTimestamp;

    public AttrValueHistoryDto(String label, String value, Instant responseTimestamp) {
        this.label = label;
        this.value = value;
        this.responseTimestamp = responseTimestamp;
    }

    public String getLabel() {
        return label;
    }

    public String getValue() {
        return value;
    }

    public Instant getResponseTimestamp() {
        return responseTimestamp;
    }
}
