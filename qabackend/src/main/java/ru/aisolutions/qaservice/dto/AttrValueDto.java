package ru.aisolutions.qaservice.dto;

public class AttrValueDto {
    private String label;
    private String value;

    public AttrValueDto(String label, String value) {
        this.label = label;
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public String getValue() {
        return value;
    }
}
