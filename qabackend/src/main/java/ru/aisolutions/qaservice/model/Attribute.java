package ru.aisolutions.qaservice.model;

import jakarta.persistence.*;

@Entity
@Table(name = "attribute")
public class Attribute {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Integer id;

  @Column(name = "question_wording", nullable = false)
  private String questionWording;

  @Column(name = "question_payload", nullable = false)
  private String questionPayload;

  @Column(name = "label", nullable = false)
  private String label;

  @Enumerated(EnumType.STRING)
  @Column(name = "type")
  private AttributeType attributeType;

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public String getQuestionPayload() {
    return questionPayload;
  }

  public void setQuestionPayload(String questionPayload) {
    this.questionPayload = questionPayload;
  }

  public String getQuestionWording() {
    return questionWording;
  }

  public void setQuestionWording(String questionWording) {
    this.questionWording = questionWording;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public AttributeType getAttributeType() {
    return attributeType;
  }

  public void setAttributeType(AttributeType attributeType) {
    this.attributeType = attributeType;
  }
}