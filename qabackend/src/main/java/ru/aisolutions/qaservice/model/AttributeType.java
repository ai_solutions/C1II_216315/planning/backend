package ru.aisolutions.qaservice.model;

public enum AttributeType {
    raw_value, calculated_value
}
