package ru.aisolutions.qaservice.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.time.Instant;

@Entity
@Table(name = "attr_value")
public class AttrValue {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Long id;

  @Column(name = "user_id", nullable = false)
  private String userId;

//  @ManyToOne(fetch = FetchType.LAZY, optional = false)
//  @JoinColumn(name = "attr_id", nullable = false)
  @Column(name = "attr_id", nullable = false)
  private Integer attrId;

  @Column(name = "value", nullable = false)
  private String value;

  @Column(name = "response_timestamp",nullable = false)
  private Instant responseTimestamp;

  public AttrValue() {
  }

  public AttrValue(String userId, Integer attrId, String value) {
    this.userId = userId;
    this.attrId = attrId;
    this.value = value;
  }

  public Instant getResponseTimestamp() {
    return responseTimestamp;
  }

  public void setResponseTimestamp(Instant responseTimestamp) {
    this.responseTimestamp = responseTimestamp;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public Integer getAttrId() {
    return attrId;
  }

  public void setAttrId(Integer attr) {
    this.attrId = attr;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
}