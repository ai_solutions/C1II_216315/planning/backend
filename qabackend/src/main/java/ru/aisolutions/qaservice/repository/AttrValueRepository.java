package ru.aisolutions.qaservice.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.aisolutions.qaservice.model.AttrValue;

@Repository
public interface AttrValueRepository extends JpaRepository<AttrValue, Long> {
  @Query(value = "select distinct on (attr_id) * from attr_value a where a.user_id=:user_id order by attr_id, response_timestamp desc;", nativeQuery = true)
  List<AttrValue> getLastAttrValuesByUserId(@Param("user_id") String userId);

  @Query(value = "select * from attr_value a where a.user_id=:user_id and a.attr_id=:attr_id ;", nativeQuery = true)
  List<AttrValue> getAttrValueHistoryByAttrId(@Param("user_id") String userId, @Param("attr_id") Integer attrId);

  @Query(value = "insert into attr_value (user_id, attr_id, \"value\") values (:user_id, :attr_id, :value) ;", nativeQuery = true)
  @Transactional
  @Modifying
  void insertAttrValue(@Param("user_id") String userId, @Param("attr_id") Integer attrId, @Param("value") String attrValue);


}
