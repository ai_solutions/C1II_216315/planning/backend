package ru.aisolutions.qaservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.aisolutions.qaservice.model.Attribute;
@Repository
public interface AttributeRepository extends JpaRepository<Attribute, Long> {
}
