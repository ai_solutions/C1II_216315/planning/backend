create type attribute_type as enum ('raw_value', 'calculated_value');

create table attribute
(
    id               serial primary key,
    question_wording text           not null,
    question_payload text           not null,
    label            text unique    not null,
    type             attribute_type not null
);

create table attr_value
(
    id                 bigserial primary key,
    user_id            character varying            not null,
    attr_id            integer references attribute not null,
    value              text                         not null,
    response_timestamp timestamp without time zone default now() not null
);


insert into attribute (id, question_wording, question_payload, label, type)
values (0, 'название', '{}', '', '');
